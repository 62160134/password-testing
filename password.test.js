const { checkLength, checkAlphabet, checkSymbol, checkDigit, checkPassword } = require('./password')

describe('Test Password Length', () => {
  test('should 8 characters to be true', () => {
    expect(checkLength('12345678')).toBe(true)
  })

  test('should 7 characters to be false', () => {
    expect(checkLength('1234567')).toBe(false)
  })

  test('should 25 characters to be true', () => {
    expect(checkLength('1111111111111111111111111')).toBe(true)
  })

  test('should 26 characters to be false', () => {
    expect(checkLength('11111111111111111111111111')).toBe(false)
  })
})

describe('Test Alphabet', () => {
  test('should has alphabet m in password', () => {
    expect(checkAlphabet('m')).toBe(true)
  })

  test('should has alphabet A in password', () => {
    expect(checkAlphabet('A')).toBe(true)
  })

  test('should has alphabet Z in password', () => {
    expect(checkAlphabet('Z')).toBe(true)
  })

  test('should has not alphabet in password', () => {
    expect(checkAlphabet('1111')).toBe(false)
  })
})

describe('Test Symbol', () => {
  test('should has symbol ! to be true', () => {
    expect(checkSymbol('11!11')).toBe(true)
  })

  test('should has symbol ~ to be true', () => {
    expect(checkSymbol('11~11~')).toBe(true)
  })

  test('should has not symbols to be false', () => {
    expect(checkSymbol('1111')).toBe(false)
  })
})

describe('Test Digit', () => {
  test('should has number 9 to be true', () => {
    expect(checkDigit('ac9b')).toBe(true)
  })

  test('should has number 3 to be true', () => {
    expect(checkDigit('dfg3')).toBe(true)
  })

  test('should has not digits to be false', () => {
    expect(checkDigit('abc')).toBe(false)
  })
})

describe('Test Password', () => {
  test('should password abc#1234 to be true', () => {
    expect(checkPassword('cde@1234')).toBe(true)
  })

  test('should password cde12345 to be false', () => {
    expect(checkPassword('cde12345')).toBe(false)
  })

  test('should password @1234567 to be false', () => {
    expect(checkPassword('@1234567')).toBe(false)
  })

  test('should password cde@123 to be false', () => {
    expect(checkPassword('cde@123')).toBe(false)
  })
})
