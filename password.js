const checkLength = (password) => {
  return password.length >= 8 && password.length <= 25
}

const checkAlphabet = (password) => {
  // const alphabets = 'abcdefghijklmnopqrstuvwxyz'
  // for (const ch of password) {
  //   if (alphabets.includes(ch.toLowerCase())) return true
  // }
  // return false
  return /[a-zA-Z]/.test(password)
}

const checkDigit = (password) => {
  const digits = '0123456789'
  for (const ch of password) {
    if (digits.includes(ch.toLowerCase())) return true
  }
  return false
}

const checkSymbol = (password) => {
  const symbols = '!"#$%&()*+,-./:;<=>?@[]^_`{|}~'
  for (const ch of password) {
    if (symbols.includes(ch.toLowerCase())) return true
  }
  return false
}

const checkPassword = (password) => {
  return checkLength(password) &&
  checkAlphabet(password) &&
  checkDigit(password) &&
  checkSymbol(password)
}

module.exports = {
  checkLength,
  checkAlphabet,
  checkDigit,
  checkSymbol,
  checkPassword
}
